```
docker run -p 8086:8086 \
  --name influxdb2 \
  -v /home/patryk/Documents/apps/pvmonitor/influx:/var/lib/influxdb \
  -v /home/patryk/Documents/apps/pvmonitor/influx2:/var/lib/influxdb2 \
  -e DOCKER_INFLUXDB_INIT_MODE=upgrade \
  -e DOCKER_INFLUXDB_INIT_USERNAME=admin \
  -e DOCKER_INFLUXDB_INIT_PASSWORD=password \
  -e DOCKER_INFLUXDB_INIT_ORG=SuaziLab \
  -e DOCKER_INFLUXDB_INIT_BUCKET=pvmonitor \
  influxdb:2.2
  ```