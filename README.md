# Simple app to read data from pvmonitor (pvmonitor.pl) devices and Fronius

## 1. Data
To store data `influxDB` 2.6 is used

**Creating DB**

1. Prepare volume and start **`influxdb2`** container. In my case it is direcotory in filesystem
```sh
docker run -d -p 8086:8086 \
      -v $PWD/data:/var/lib/influxdb2 \
      -v $PWD/config:/etc/influxdb2 \
      -e DOCKER_INFLUXDB_INIT_MODE=setup \
      -e DOCKER_INFLUXDB_INIT_USERNAME=my-user \
      -e DOCKER_INFLUXDB_INIT_PASSWORD=my-password \
      -e DOCKER_INFLUXDB_INIT_ORG=my-org \
      -e DOCKER_INFLUXDB_INIT_BUCKET=my-bucket \
      influxdb:2.6
```

2. Connect to DB via web browser and create API keys in Data -> API Tokens.
API Token will be needed to send and query data


### Data collection

**Counter data - pvmonitor**

|Kind of data|pvmonitor API filed|My API field|
|---|---|---|
|Power|`em_P`|`Power`|
|Power Phase 1|`em_P L1`|`P_L1`|
|Power Phase 2|`em_P L2`|`P_L2`|
|Power Phase 3|`em_P L3`|`P_L3`|
|Current Phase 1|`em_I L1`|`I_L1`|
|Current Phase 2|`em_I L2`|`I_L2`|
|Current Phase 3|`em_I L3`|`I_L3`|
|Voltage Phase 1|`em_V L1`|`V_L1`|
|Voltage Phase 2|`em_V L2`|`V_L2`|
|Voltage Phase 3|`em_V L3`|`V_L3`|
|Sum of exported engergy|`em_Exp kWh`|`En_exp`|
|Sum of imported engergy|`em_Imp kWh`|`En_imp`|

**Fronius data - pvmonitor**

|Kind of data|pvmonitor API filed|My API field|
|---|---|---|
|Current Power Generation|`em_P gen`|`pv_P`|
|Current Power String 1 - morning|`em_P DC1`|`pv_S1`|
|Current Power String 2 - afternoon|`em_P DC2`|`pv_S2`|
|Energy generated since launch|`em_En gen`|`pv_E_sum`|


## 2. Application

This application queries local pvmonitor web instance for data from electric counter and Fronius pv installation. Then data is sent to InfluxDB to be presented in Grafana dashboard.
Hardware and software available under: `pvmonitor.pl`


**Hardware compatibility**
App is compatible with **DTSU666** and **Fronius (Sunspec)** devices.
For other devices, verify response json from pvmonitor and adjust code to your needs. Example below:</br>
*Lines 18-22 of `code/app.py`*
```python
    for d in rs485_data['emetersread']:
        if d['em_Typ'] == "DTSU666":
            hint_data = d
        if d['em_Typ'] == "Fronius (Sunspec)":
            fronius_data = d
```

Code of application is available under `./code` directory.

**Docker build**
```sh
docker build -t suazi/pvmonitorapp:[version_tag] .
```

**Push to repo**
```sh
docker push suazi/pvmonitorapp:[version_tag]
```

**Manual run**

All Env variables must have assigned correct values for correct app operation

```sh
docker run -d --name pvmonitorapp \
--env PVMONITOR_HOST= \
--env PVMONITOR_PORT= \
--env INFLUX_TOKEN= \
--env INFLUX_ORG= \
--env INFLUX_BUCKET= \
--env INFLUX_HOST= \
--env INFLUX_PORT= \
suazi/pvmonitorapp:1.1.0
```

|Env Variable|Description|
|---|---|
|PVMONITOR_HOST|IP or Hostname of pvmonitor webpage. App will query `/public` endpoint for json data.|
|PVMONITOR_PORT|Port of pvmonitor webpage|
|INFLUX_TOKEN|API Token with privileges to write to given bucket|
|INFLUX_ORG|InfluxDB organization where data resides|
|INFLUX_BUCKET|InfluxDB bucket for data|
|INFLUX_HOST|Name or IP address of database|
|INFLUX_PORT|Port of database|
|LOGGING_LEVEL|If set to `debug` - enables debug logging.|

**Debug logging**
To enable debug logging, set optional `LOGGING_LEVEL` env variable to: `debug` and run application

## 3. Grafana

Grafana running in container uses user with id: `472`.
Volume must be writable by this user.

1. Prepare volume and start **`grafana`** container.
```sh
sudo chown -R 472:472 /path/to/grafana/volume
docker run -d -p 3000:3000 --name=grafana -v /path/to/grafana/volume:/var/lib/grafana  grafana/grafana-oss
```
Default credentials are: `admin`/`admin`

Grafana dashboard json is available in `grafana.json` file.

**Dashboard view (Titles are in Polish language)**
![grafana1](./img/Selection_007.png)
![grafana2](./img/Selection_008.png)


## Influx v2 queries

### CLI
**Data removal**

Use admin token

```
influx config create --config-name <config name> \
  --host-url http://localhost:8086 \
  --org SuaziLab \
  --token <TOKEN> \
  --active

influx delete --org SuaziLab --bucket pvmonitor \
  --start '2022-05-16T06:58:37.000Z' \
  --stop '2022-05-16T06:58:39.000Z' \
  --predicate '_measurement="hint" AND type="fronius"'
```

### Flux

_Below queries might not be up to date as they are evolving quickly_

**Dzienna produkcja energii**
```influx
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  |> range(start: experimental.subDuration(d: 10d, from: today()))
  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "pv_E_sum")
  |> filter(fn: (r) => r["type"] == "fronius")
  |> aggregateWindow(every: 1d, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```

**Dzienny import energii**
```influx
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  |> range(start: experimental.subDuration(d: 10d, from: today()))
  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "En_imp")
  |> filter(fn: (r) => r["type"] == "hint")
  |> aggregateWindow(every: 1d, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```

**Dzienny eksport energii**
```influx
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  |> range(start: experimental.subDuration(d: 10d, from: today()))
  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "En_exp")
  |> filter(fn: (r) => r["type"] == "hint")
  |> aggregateWindow(every: 1d, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```
---
**Miesięczna Produkcja - dla 2022**
```
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  // Hardcoded start of the year
  |> range(start: 2021-12-31T23:00:00Z)  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "pv_E_sum")
  |> filter(fn: (r) => r["type"] == "fronius")
  |> aggregateWindow(every: 1mo, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```
**Miesięczny Import - dla 2022**
```
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  // Hardcoded start of the year
  |> range(start: 2021-12-31T23:00:00Z)  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "En_imp")
  |> filter(fn: (r) => r["type"] == "hint")
  |> aggregateWindow(every: 1mo, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```
**Miesięczny Export - dla 2022**
```
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  // Hardcoded start of the year
  |> range(start: 2021-12-31T23:00:00Z)  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "En_exp")
  |> filter(fn: (r) => r["type"] == "hint")
  |> aggregateWindow(every: 1mo, fn: spread, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> top(n: 10, columns: ["_time"] )
  |> sort(columns: ["_time"], desc: false)
  |> yield(name: "Import")
```
---
**Produkcja chwilowa - wykres**
Relative time: `now/d`
```
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "pv_P")
  |> filter(fn: (r) => r["type"] == "fronius")
  |> aggregateWindow(every: 1m, fn: mean, createEmpty: false, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> yield()
```
**Zużycie w ciągu dnia - per faza**
```
import "timezone"
import "experimental"

from(bucket: "pvmonitor")
  // Hardcoded for summer time in EU
  |> range(start: experimental.subDuration(d: 4h, from: today()))
  |> filter(fn: (r) => r["_measurement"] == "hint")
  |> filter(fn: (r) => r["_field"] == "pv_P")
  |> filter(fn: (r) => r["type"] == "fronius")
  |> aggregateWindow(every: 1m, fn: mean, createEmpty: false, timeSrc: "_start", location: timezone.location(name: "Europe/Warsaw"))
  |> yield()
```


