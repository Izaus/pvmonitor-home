from datetime import datetime
import requests
import json
from time import sleep
from pprint import pprint
from os import getenv
import influxdb_client
import sys
from random import randint

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

##### LOAD ENV VARIABLES #####
# pvmonitor
PVMONITOR_HOST = getenv('PVMONITOR_HOST')
PVMONITOR_PORT = int(getenv('PVMONITOR_PORT', 80))
HIDE_TOKEN = getenv('HIDE_TOKEN', True) # Hides Influx token when debug logging is enabled

# influxDB
INFLUX_TOKEN = getenv('INFLUX_TOKEN')
INFLUX_ORG = getenv('INFLUX_ORG')
INFLUX_BUCKET = getenv('INFLUX_BUCKET')
INFLUX_HOST = getenv('INFLUX_HOST')
INFLUX_PORT = int(getenv('INFLUX_PORT', 80))
##############################

##### LOGGING ######
import logging
# create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)
####################

logger.info("###### Starting Application ######")

def get_pv_data(host, port):
    logger.debug(f'Connecting to PV-terminal at {host}:{port}')
    url = f"http://{host}:{port}/public"
    response = requests.get(url)
    if response.status_code == 200:
        logger.debug(f'PV-terminal response status: {response.status_code}')
    else:
        logger.warning(f'PV-terminal response status: {response.status_code}')

    data = response.json()
    rs485_data = data['rs485']

    fronius_data = {}
    # watt-hour meater
    hint_data = {}
    for d in rs485_data['emetersread']:
        if d['em_Typ'] == "DTSU666":
            hint_data = d
        if d['em_Typ'] == "Fronius (Sunspec)":
            fronius_data = d

    return {"fronius": fronius_data,
            "hint": hint_data}

def hint_parser(pv_hint_data):
    """The expected dict structure by Point.from_dict() is:
    |          - measurement
    |          - tags
    |          - fields
    |          - time (not required)
    """
    logger.debug(f'Parsing hint counter data')
    hint_parser_data = {
        "measurement": "hint",
        "tags": {"type": "hint"},
        "fields": {
            "Power": float(pv_hint_data['em_P']),
            "P_L1": float(pv_hint_data['em_P L1']),
            "P_L2": float(pv_hint_data['em_P L2']),
            "P_L3": float(pv_hint_data['em_P L3']),
            "I_L1": float(pv_hint_data['em_I L1']),
            "I_L2": float(pv_hint_data['em_I L2']),
            "I_L3": float(pv_hint_data['em_I L3']),
            "V_L1": float(pv_hint_data['em_V L1']),
            "V_L2": float(pv_hint_data['em_V L2']),
            "V_L3": float(pv_hint_data['em_V L3']),
            "En_exp": float(pv_hint_data['em_Exp kWh']),
            "En_imp": float(pv_hint_data['em_Imp kWh'])
        }
    }
    return hint_parser_data

def fronius_parser(pv_fronius_data):
    """The expected dict structure by Point.from_dict() is:
    |          - measurement
    |          - tags
    |          - fields
    |          - time (not required)
    """
    logger.debug(f'Parsing fronius data')
    fronius_parser_data = {
        "measurement": "hint",
        "tags": {"type": "fronius"},
        "fields": {
            "pv_P": float(pv_fronius_data['em_P gen']),
            "pv_E_sum": float(pv_fronius_data['em_En gen']),
            "pv_S1": float(pv_fronius_data['em_P DC1']),
            "pv_S2": float(pv_fronius_data['em_P DC2'])
        }      
    }
    return fronius_parser_data

def main():
    logger.debug("###### Loading env variables ######")
    logger.debug(f'PVMONITOR_HOST={PVMONITOR_HOST}')
    logger.debug(f'PVMONITOR_PORT={PVMONITOR_PORT}')
    if HIDE_TOKEN:
        logger.debug(f'INFLUX_TOKEN=REDACTED')
    else:
        logger.debug(f'INFLUX_TOKEN={INFLUX_TOKEN}')
    logger.debug(f'INFLUX_ORG={INFLUX_ORG}')
    logger.debug(f'INFLUX_BUCKET={INFLUX_BUCKET}')
    logger.debug(f'INFLUX_HOST={INFLUX_HOST}')
    logger.debug(f'INFLUX_PORT={INFLUX_PORT}')
    # logger.debug(f'LOGGING_LEVEL={LOGGING_LEVEL}')
    logger.debug("###################################")
    with InfluxDBClient(url=f"{INFLUX_HOST}:{INFLUX_PORT}", token=INFLUX_TOKEN, org=INFLUX_ORG) as client:
        logger.info(f'Connected to InfluxDB at: {INFLUX_HOST}:{INFLUX_PORT}. Org: {INFLUX_ORG}')
        write_api = client.write_api(write_options=SYNCHRONOUS)
        logger.info(f'Staring application loop')
        while True:
            try:
                pvmonitor_data = get_pv_data(PVMONITOR_HOST, PVMONITOR_PORT)
                hint_data = hint_parser(pvmonitor_data['hint'])
                fronius_data = fronius_parser(pvmonitor_data['fronius'])
                if hint_data.get('fields').get('En_exp') != 0 or hint_data('fields').get('En_imp') != 0:
                    logger.debug(f'Correct hint data received - proceeding')
                    point = Point.from_dict(hint_data)
                    write_api.write(INFLUX_BUCKET, INFLUX_ORG, point)
                    logger.debug(f'Written hint data to DB')
                else:
                    logger.warning(f'Incorrect hint data received- En_exp: {hint_data["En_exp"]}, En_imp: {hint_data["En_imp"]} - skipping')
                
                if fronius_data.get('fields').get('pv_E_sum') > 0.0 and fronius_data.get('fields').get('pv_E_sum') < 2147481.0:
                    logger.debug(f'Correct fronius data received - proceeding')
                    point = Point.from_dict(fronius_data)
                    write_api.write(INFLUX_BUCKET, INFLUX_ORG, point)
                    logger.debug(f'Written fronius data to DB')
                else:
                    logger.warning(f'Incorrect fronius data received - pv_E_sum: {fronius_data["pv_E_sum"]} - skipping')

                sleep(60)        
            except Exception as e:
                logger.exception("Exception caught:")
                sleep(30)
                pass


if __name__ == "__main__":
    main()
